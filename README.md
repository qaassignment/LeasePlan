# What was done:

1. Renamed classes and methods according to naming convention
2. Renamed steps to better match what should be done
3. Rewrote steps so they do what is described
4. Moved files across the project for better visibility
5. Cleaned project: deleted unnecessary files, comments, etc.
6. Moved all dependencies from gradle to maven, deleted unused dependencies
7. Added logging
8. Added html report generation

# What wasn't done:
1. Usually logic has two layers: business and application, but for such small projects it's unnecessary
2. Usually projects have advanced config files with environments and users, but here URI is hardcoded in WaarkoopEndpoint

# Run instructions:
## Locally:
Use **mvn clean verify** to run tests locally (given local machine has java and maven installed)
This will run tests and generate html report in /target/site/serenity/index.html

## Gitlab:
If you are a contributor: go to https://gitlab.com/qaassignment/LeasePlan/-/pipelines and click **Run pipeline**.
HTML report is available under **Artifacts -> Download artifacts**. After downloading,
open target/site/serenity/index.html in browser. 
If you are not a contributor, you still can see previous runs and download artifacts or fork a project and run
fork by yourself.
