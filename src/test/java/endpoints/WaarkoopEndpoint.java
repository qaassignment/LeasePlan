package endpoints;

public enum WaarkoopEndpoint {
    BASE("https://waarkoop-server.herokuapp.com/"),
    SEARCH("api/v1/search/test/");
    private final String url;

    WaarkoopEndpoint(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
}
