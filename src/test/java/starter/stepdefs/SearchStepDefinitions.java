package starter.stepdefs;

import endpoints.WaarkoopEndpoint;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.rest.SerenityRest;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;

public class SearchStepDefinitions {

    private static final String BASE_URI = WaarkoopEndpoint.BASE.getUrl();
    private static final String SEARCH = WaarkoopEndpoint.SEARCH.getUrl();

    @When("User searches for {string}")
    public void userSearchesFor(String item) {
        String URL = BASE_URI + SEARCH + item;
        SerenityRest.get(URL).then().log().ifError();
    }

    @Then("User receives status code {string}")
    public void userReceivesStatusCode(String statusCode) {
        SerenityRest.restAssuredThat(response -> {
            response.statusCode(Integer.parseInt(statusCode));
            response.log().ifValidationFails();
        });
    }

    @Then("User can see {string}")
    public void userCanSeeTitle(String key) {
        SerenityRest.restAssuredThat(response -> {
            response.body(containsString(key));
            response.log().ifValidationFails();
        });
    }

    @Then("User can not see results")
    public void userCantSeeResults() {
        SerenityRest.restAssuredThat(response -> {
            response.body("detail.message", equalTo("Not found"));
            response.log().ifValidationFails();
        });
    }
}
