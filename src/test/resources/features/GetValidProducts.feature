Feature: Search for existing items

  Scenario Outline: Verify search results for valid products
    When User searches for "<Item>"
    Then User receives status code "<StatusCode>"
    And User can see "<Key>"

    Examples:
      | Item  | StatusCode | Key      |
      | apple | 200        | provider |
      | mango | 200        | title    |
      | tofu  | 200        | url      |
      | water | 200        | brand    |