Feature: Search for non-existing items

  Scenario Outline: Verify search results for invalid products
    When User searches for "<Item>"
    Then User receives status code "<StatusCode>"
    And User can not see results

    Examples:
      | Item  | StatusCode |
      | tram  | 404        |
      | bus   | 404        |
      | plane | 404        |
      | boat  | 404        |
